module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = req.query.q
    let answer = question || 'Hello Hackathon'

    if (/what is your name/.test(question)) {
      answer = 'Rafal'
    }

    if (/which of the following numbers is the largest/.test(question)) {
      let numbers = question
        .split(':')
        .pop()
        .trim()
        .split(', ')
        .map(n => parseInt(n, 10))

      answer = Math.max(...numbers)
    }

    if (/what is(.*)plus/.test(question)) {
      answer = question
        .slice(9)
        .trim()
        .replace('what is', '')
        .split('plus')
        .reduce((i, n) => i + parseInt(n, 10), 0)
    }

    if (/what is(.*)multiplied by/.test(question)) {
      answer = question
        .slice(9)
        .trim()
        .replace('what is', '')
        .split('multiplied by')
        .reduce((i, n) => i * parseInt(n, 10), 1)
    }

    if (/which of the following numbers are primes/.test(question)) {
      const isPrime = num => {
        for (let i = 2; i < num; i++) if (num % i === 0) return false
        return num > 1
      }

      let prime = []

      question
        .split(':')
        .pop()
        .trim()
        .split(', ')
        .forEach(n => {
          if (isPrime(n)) {
            prime.push(n)
          }
        })

      answer = prime.join(',')
    }

    let tmp1 = question

    if (tmp1 && /theresa may first elected as the prime minister/.test(tmp1.toLowerCase())) {
      answer = '2016'
    }

    let tmp2 = question

    if (tmp2 && /james bond in the film dr no/.test(tmp2.toLowerCase())) {
      answer = 'Sean Connery'
    }

    if (/Fibonacci sequence/.test(question)) {
      const fib = n => {
        var a = 1,
          b = 0,
          temp

        while (n >= 0) {
          temp = a
          a = a + b
          b = temp
          n--
        }

        return b
      }

      let number = question
        .split(':')
        .pop()
        .trim()
        .replace(/[^0-9]/g, '')

      answer = fib(number - 1)
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    // Answer the question
    res.status(200).send(String(answer))
  }
}
