const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  it('should say my name', done => {
    agent
      .get('/api')
      .query({ q: 'daab2ea0: what is your name' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Rafal')
        done()
      })
  })

  it('should be larges number', done => {
    agent
      .get('/api')
      .query({ q: '0b2e5c10: which of the following numbers is the largest: 464, 9, 909, 78' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('909')
        done()
      })
  })

  it('should be result of plus', done => {
    agent
      .get('/api')
      .query({ q: '0b2e5c10: what is 5 plus 19' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('24')
        done()
      })
  })

  it('should be result of multiplied', done => {
    agent
      .get('/api')
      .query({ q: 'a4c51270: what is 16 multiplied by 4' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('64')
        done()
      })
  })

  it('should be result of primes', done => {
    agent
      .get('/api')
      .query({ q: '8bf56670: which of the following numbers are primes: 281, 197, 310, 14' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('281,197')
        done()
      })
  })
  it('should be result of both a square and a cube', done => {
    agent
      .get('/api')
      .query({ q: '2a6b6fd0: which year was Theresa May first elected as the Prime Minister of Great Britain' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('2016')
        done()
      })
  })

  it('should be result who played James Bond in the film Dr No', done => {
    agent
      .get('/api')
      .query({ q: '4c6c4890: who played James Bond in the film Dr No' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Sean Connery')
        done()
      })
  })
  it('should be result of n number in the Fibonacci sequence', done => {
    agent
      .get('/api')
      .query({ q: 'c076dc90: what is the 5th number in the Fibonacci sequence' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('5')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
