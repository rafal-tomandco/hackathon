#!/usr/bin/env node

const http = require('http')
const debug = require('debug')(require('../package.json').name)

// App
const app = require('../app')

/* Revealing module pattern
 * ensures all methods and variables are kept private until explicitly exposed
 */
const runner = (() => {
  // Normalize a port into a number, string, or false.
  const normalisePort = value => {
    const port = parseInt(value, 10)

    if (isNaN(port)) {
      return value // named pipe
    }

    if (port >= 0) {
      return port // port number
    }

    return false
  }

  // Event listener for HTTP server "listening" event.
  const onListening = () => {
    const address = server.address()
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + address.port
    debug('Listening on ' + bind)
  }

  // Event listener for HTTP server "error" event.
  const onError = error => {
    if (error.syscall !== 'listen') {
      throw error
    }

    const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

    // Handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        // eslint-disable-next-line no-console
        console.error(bind + ' requires elevated privileges')
        process.exit(1)
        break
      case 'EADDRINUSE':
        // eslint-disable-next-line no-console
        console.error(bind + ' is already in use')
        process.exit(1)
        break
      default:
        throw error
    }
  }

  return {
    normalisePort: normalisePort,
    onListening: onListening,
    onError: onError
  }
})()

// Set port
const port = runner.normalisePort(process.env.PORT || '3000')
app.set('port', port)

// Create webserver and serve the app
const server = http.createServer(app)
server.listen(port)
server.on('error', runner.onError)
server.on('listening', runner.onListening)
